﻿using UnityEngine;

public interface IPooledCallbacks
{
    void OnEnterPool();
    void OnExitPool();
}

public class Pooled : MonoBehaviour
{
    public Pool OwnerPool { get; set; }
    private IPooledCallbacks[] m_PoolCallbacks;

    private void Awake()
    {
        m_PoolCallbacks = GetComponents<IPooledCallbacks>();
    }

    public void SendBackToPool()
    {
        OwnerPool.PutObject(this);
    }

    public void NotifyEnterPool()
    {
        if (m_PoolCallbacks != null)
        {
            foreach (IPooledCallbacks callbacks in m_PoolCallbacks)
            {
                callbacks.OnEnterPool();
            }
        }
    }

    public void NotifyExitPool()
    {
        if (m_PoolCallbacks != null)
        {
            foreach (IPooledCallbacks callbacks in m_PoolCallbacks)
            {
                callbacks.OnExitPool();
            }
        }
    }
}