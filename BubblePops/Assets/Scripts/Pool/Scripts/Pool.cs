﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class Pool : MonoBehaviour
{
	public enum EPoolPolicy
	{
		FirstAvailable = 0,
		RoundRobin = 1
	}

	[SerializeField] private GameObject m_Prefab = null;
	[SerializeField] private EPoolPolicy m_Policy = EPoolPolicy.FirstAvailable;
	[SerializeField] private int m_MaxCapacity = 1000;
	[SerializeField] private int m_PreloadAmount = 0;

	public UnityEvent OnMaxCapacityReached;

	// Used by custom inspector to show real-time pool state in play mode
	[SerializeField] private List<Pooled> m_UnavailableObjects = new List<Pooled>();
	[SerializeField] private List<Pooled> m_AvailableObjects = new List<Pooled>();

	private int m_AvailableObjectsIndex = 0;

	private bool registered = false;

	public int MaxCapacity
	{
		get { return m_MaxCapacity; }
		set
		{
			if (value < CurrentCapacity)
			{
				Debug.LogWarning("Cannot reduce MaxCapacity under current capacity (" + CurrentCapacity + ").");
			}
			else
			{
				m_MaxCapacity = value;
			}
		}
	}

	public int CurrentCapacity => m_UnavailableObjects.Count + m_AvailableObjects.Count;

	public EPoolPolicy Policy
	{
		get { return m_Policy; }
		set { m_Policy = value; }
	}

	public GameObject Prefab
	{
		get
		{
			return m_Prefab;
		}
		set
		{
			if (!registered)
			{
				m_Prefab = value;
				Init(m_PreloadAmount);
			}
			else
			{
				Debug.LogWarning("Cannot change prefab while pool is active.");
			}
		}
	}

	void Awake()
	{
		if (!registered)
		{
			Init(m_PreloadAmount);
		}
	}

	public void Init(int preloadAmount = 0)
	{
		if (m_Prefab != null)
		{
			Pools.RegisterPool(this);
			registered = true;

			Load(preloadAmount);
		}
	}

	public void Reset()
	{
		Pools.UnregisterPool(this);
		registered = false;

		m_AvailableObjects.Clear();
		m_UnavailableObjects.Clear();
	}

	public GameObject GetObject()
	{
		Pooled pooledObj = GetFromm_AvailableObjects();

		if (pooledObj != null)
		{
			m_AvailableObjects.RemoveAt(m_AvailableObjectsIndex);
		}
		else if (CurrentCapacity < m_MaxCapacity || m_MaxCapacity == 0)
		{
			pooledObj = CreateNewObject();
		}

		if (pooledObj != null)
		{
			pooledObj.gameObject.SetActive(true);
			m_UnavailableObjects.Add(pooledObj);

			pooledObj.NotifyExitPool();

			return pooledObj.gameObject;
		}

		return null;
	}

	private Pooled CreateNewObject()
	{
		Pooled pooled = null;

		if (m_Prefab != null)
		{
			GameObject pooledObj = Instantiate(m_Prefab);
			pooledObj.transform.SetParent(transform, false);
			pooledObj.SetActive(false);

			pooled = pooledObj.AddComponent<Pooled>();
			pooled.OwnerPool = this;
		}

		if (MaxCapacity > 0 && CurrentCapacity + 1 == MaxCapacity)
		{
			Debug.LogWarning("Max capacity reached in Pool '" + gameObject.name + "'");
			OnMaxCapacityReached.Invoke();
		}

		return pooled;
	}

	private Pooled GetFromm_AvailableObjects()
	{
		Pooled pooledObj = null;

		while (pooledObj == null && m_AvailableObjects.Count > 0)
		{
			UpdateIndexByPolicy();

			pooledObj = m_AvailableObjects[m_AvailableObjectsIndex];
			if (pooledObj == null)
			{
				Debug.LogWarning("An object from Pool '" + gameObject.name + "' has been externally destroyed.");
				m_AvailableObjects.RemoveAt(m_AvailableObjectsIndex);
				m_AvailableObjectsIndex = Mathf.Max(m_AvailableObjectsIndex - 1, 0);
			}
			else
			{
				break;
			}
		}

		return pooledObj;
	}

	private void UpdateIndexByPolicy()
	{
		switch (m_Policy)
		{
			case EPoolPolicy.FirstAvailable:
				m_AvailableObjectsIndex = 0;
				break;

			case EPoolPolicy.RoundRobin:
				m_AvailableObjectsIndex++;
				if (m_AvailableObjectsIndex >= m_AvailableObjects.Count)
				{
					m_AvailableObjectsIndex = 0;
				}
				break;
		}
	}

	public bool PutObject(GameObject gameObject)
	{
		return PutObject(gameObject.GetComponent<Pooled>());
	}

	public bool PutObject(Pooled pooled)
	{
		if (pooled.OwnerPool == this)
		{
			GameObject obj = pooled.gameObject;
			Transform objTransform = obj.transform;
			objTransform.SetParent(transform, false);
			obj.SetActive(false);

			pooled.NotifyEnterPool();

			m_AvailableObjects.Add(pooled);
			m_UnavailableObjects.Remove(pooled);

			return true;
		}
		return false;
	}

	public void Load(int amount)
	{
		if (amount <= 0)
		{
			return;
		}

		int currentCapacity = CurrentCapacity;
		int amountToLoad = Mathf.Min(currentCapacity + amount, MaxCapacity) - currentCapacity;
		for (int i = 0; i < amountToLoad; i++)
		{
			Pooled pooled = CreateNewObject();
			m_AvailableObjects.Add(pooled);
		}
	}

	void OnDestroy()
	{
		Reset();
	}
}