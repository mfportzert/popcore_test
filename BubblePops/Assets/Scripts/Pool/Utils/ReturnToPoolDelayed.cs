﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToPoolDelayed : MonoBehaviour
{
    [SerializeField] private float m_Delay = 1f;

    private float m_RemainingDelay = 0f;

    private void OnEnable()
    {
        m_RemainingDelay = m_Delay;
    }

    private void Update()
    {
        if (m_RemainingDelay > 0f)
        {
            m_RemainingDelay -= Time.deltaTime;
            if (m_RemainingDelay <= 0f)
            {
                ReturnToPool();
            }
        }
    }

    private void ReturnToPool()
    {
        Pools.Send(gameObject);

        m_RemainingDelay = 0f;
    }
}
