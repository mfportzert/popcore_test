﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ParticleSystem))]
public class ReturnToPoolOnFxEnd : MonoBehaviour
{
    private ParticleSystem m_ParticleSystem = null;

    private void Awake()
    {
        m_ParticleSystem = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (!m_ParticleSystem.IsAlive())
        {
            Pools.Send(gameObject);
        }
    }
}