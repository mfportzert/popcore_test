using System.Collections;
using UnityEngine;

namespace Pink.Utils
{
    public class ForceFrameRate : MonoBehaviour
    {
        public int target = 60;

        void Start()
        {
            // Initialisation
            QualitySettings.vSyncCount = 0;
        }


        void Update()
        {
            if (target != Application.targetFrameRate)
            {
                Application.targetFrameRate = target;
            }
        }

    }
}