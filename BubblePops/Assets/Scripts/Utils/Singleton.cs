﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    [SerializeField] private bool m_DontDestroy = false;

    protected static T s_Instance;
    private static bool s_ShuttingDown = false;
    private static object s_Lock = new object();

    protected virtual void Awake()
    {
        if (s_Instance != null && s_Instance != this)
        {
            GameObject.Destroy(gameObject);
        }

        if (m_DontDestroy)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public static T Instance
    {
        get
        {
            if (s_ShuttingDown)
            {
                //Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                //"' already destroyed. Returning null.");
                return null;
            }

            lock (s_Lock)
            {
                if (s_Instance == null)
                {
                    s_Instance = (T)FindObjectOfType(typeof(T)) as T;

                    if (s_Instance == null)
                    {
                        GameObject singletonObj = new GameObject();
                        s_Instance = singletonObj.AddComponent<T>();
                        singletonObj.name = typeof(T).ToString() + " (Singleton)";
                    }
                }
                return s_Instance;
            }
        }
    }

    protected virtual void OnApplicationQuit()
    {
        s_ShuttingDown = true;
    }

    protected virtual void OnDestroy()
    {
        if (m_DontDestroy)
        {
            s_ShuttingDown = true;
        }
    }
}
