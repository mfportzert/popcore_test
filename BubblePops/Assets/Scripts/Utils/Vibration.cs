using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MoreMountains.NiceVibrations;

public class Vibration
{
    private static bool s_Enabled = true;

    public static void SetEnabled(bool enabled)
    {
        s_Enabled = enabled;
    }

    /// <summary>
    /// The following methods are bound (via the inspector) to buttons in the demo scene, and will call the corresponding vibration methods
    /// </summary>

    /// <summary>
    /// Triggers the default Unity vibration, without any control over duration, pattern or amplitude
    /// </summary>
    public static void TriggerUnityDefault()
    {
        if (!s_Enabled) return;

#if UNITY_IOS || UNITY_ANDROID
        Handheld.Vibrate();
#endif
    }

    /// <summary>
    /// Triggers the default Vibrate method, which will result in a medium vibration on Android and a medium impact on iOS
    /// </summary>
    public static void TriggerDefaultVibrate()
    {
        if (!s_Enabled) return;

        MMVibrationManager.Vibrate();
    }

    /// <summary>
    /// Triggers the selection haptic feedback, a light vibration on Android, and a light impact on iOS
    /// </summary>
    public static void TriggerSelection()
    {
        TriggerHaptic(HapticTypes.Selection);
    }

    /// <summary>
    /// Triggers the success haptic feedback, a light then heavy vibration on Android, and a success impact on iOS
    /// </summary>
    public static void TriggerSuccess()
    {
        TriggerHaptic(HapticTypes.Success);
    }

    /// <summary>
    /// Triggers the warning haptic feedback, a heavy then medium vibration on Android, and a warning impact on iOS
    /// </summary>
    public static void TriggerWarning()
    {
        TriggerHaptic(HapticTypes.Warning);
    }

    /// <summary>
    /// Triggers the failure haptic feedback, a medium / heavy / heavy / light vibration pattern on Android, and a failure impact on iOS
    /// </summary>
    public static void TriggerFailure()
    {
        TriggerHaptic(HapticTypes.Failure);
    }

    /// <summary>
    /// Triggers a light impact on iOS and a short and light vibration on Android.
    /// </summary>
    public static void TriggerLightImpact()
    {
        TriggerHaptic(HapticTypes.LightImpact);
    }

    /// <summary>
    /// Triggers a medium impact on iOS and a medium and regular vibration on Android.
    /// </summary>
    public static void TriggerMediumImpact()
    {
        TriggerHaptic(HapticTypes.MediumImpact);
    }

    /// <summary>
    /// Triggers a heavy impact on iOS and a long and heavy vibration on Android.
    /// </summary>
    public static void TriggerHeavyImpact()
    {
        TriggerHaptic(HapticTypes.HeavyImpact);
    }

    private static void TriggerHaptic(HapticTypes type)
    {
        if (!s_Enabled) return;

        //Debug.LogError("Create haptic: "+type.ToString());
        MMVibrationManager.Haptic(type);
    }
}
