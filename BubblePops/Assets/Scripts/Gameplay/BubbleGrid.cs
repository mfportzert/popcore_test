using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using NaughtyAttributes;

public class BubbleGrid : MonoBehaviour
{
    [SerializeField] private int m_NbColumns = 6;
    [SerializeField] private int m_InitialRows = 4;
    [SerializeField] private float m_CellSize = 1f;
    [SerializeField] private BubbleSlot m_SlotPrefab = null;

    [Header("Procedural")]
    [SerializeField] private int m_MinVisibleRows = 6; // if less, insert top one
    [SerializeField] private int m_MaxVisibleRows = 8; // if more, remove top one

    [Header("Animations")]
    [SerializeField] private float m_MoveYDuration = 1f;

    private List<BubbleRow> m_Rows = new List<BubbleRow>();
    private bool m_IsAnimating = false;
    private int m_TopRowId = -1; // increases/decreases when top rows are added/removed
    private Vector3 m_InitialLocalPos;

    public bool IsAnimating => m_IsAnimating;

    private void Start()
    {
        m_InitialLocalPos = transform.localPosition;

        ResetGrid();
    }

    public void ResetGrid()
    {
        m_TopRowId = -1;
        transform.localPosition = m_InitialLocalPos;

        foreach (var row in m_Rows)
        {
            GameObject.Destroy(row.gameObject);
        }
        m_Rows.Clear();

        for (int i = 0; i < m_InitialRows; ++i)
        {
            AddTopRow();
        }

        AddBottomRow();
    }

    private float GetRowHeight()
    {
       return m_CellSize * 0.85f;
    }

    private void AddTopRow()
    {
        BubbleRow row = CreateRow(m_TopRowId);
        row.IsTopRow = true;
        row.Fill();

        row.SetupLinks(null, (m_Rows.Count > 0) ? m_Rows[0] : null);

        if (m_Rows.Count > 0)
        {
            m_Rows[0].IsTopRow = false;
        }

        m_Rows.Insert(0, row);
        m_TopRowId++;
    }

    private void AddBottomRow()
    {
        BubbleRow row = CreateRow(m_TopRowId - (m_Rows.Count + 1));

        row.SetupLinks((m_Rows.Count > 0) ? m_Rows[m_Rows.Count - 1] : null, null);

        m_Rows.Add(row);
    }

    private BubbleRow CreateRow(int rowId)
    {
        BubbleRow row = new GameObject().AddComponent<BubbleRow>();
        row.Init(rowId, m_NbColumns);
        row.transform.SetParent(transform);
        row.transform.localPosition = new Vector3(0, rowId * GetRowHeight(), 0);

        float centeredOffset = ((m_CellSize * m_NbColumns) * 0.5f) - m_CellSize * 0.25f;
        for (int i = 0; i < m_NbColumns; ++i)
        {
            BubbleSlot slot = GameObject.Instantiate(m_SlotPrefab, row.transform);
            slot.Row = row;
            row.Slots[i] = slot;

            slot.transform.localPosition = new Vector3(
                (i * m_CellSize) + ((rowId % 2 == 0) ? m_CellSize * 0.5f : 0) - centeredOffset,
                0, 0);
        }

        return row;
    }

    private void RemoveTopRow()
    {
        m_TopRowId--;

        GameObject.Destroy(m_Rows[0].gameObject);
        m_Rows.RemoveAt(0);

        // Top row is now the roof and needs to be always filled
        m_Rows[0].IsTopRow = true;
        m_Rows[0].Fill();
    }

    private void RemoveBottomRow()
    {
        //Debug.LogError("RemoveBottomRow");
        GameObject.Destroy(m_Rows[m_Rows.Count - 1].gameObject);
        m_Rows.RemoveAt(m_Rows.Count - 1);
    }

    private bool IsBottomRowEmpty()
    {
        return (m_Rows.Count > 0) ? m_Rows[m_Rows.Count - 1].IsEmpty() : false;
    }

    public void UpdateContent()
    {
        DestroyClearedRows();

        if (!IsBottomRowEmpty())
        {
            AddBottomRow();
        }

        int nbRows = m_Rows.Count - 2; // exclude top and bottom rows
        if (nbRows < m_MinVisibleRows)
        {
            AddTopRow();
            AnimateDown();
        }
        else if (nbRows > m_MaxVisibleRows)
        {
            AnimateUp(RemoveTopRow);
        }
    }

    private void DestroyClearedRows()
    {
        int clearedCount = 0;
        for (int i = m_Rows.Count - 1; i >= 0; --i)
        {
            if (!m_Rows[i].IsEmpty())
            {
                break;
            }
            clearedCount++;
        }

        // leave at least one empty row to receive new bubbles
        for (int i = 0; i < clearedCount - 1; ++i)
        {
            RemoveBottomRow();
        }
    }

    private void AnimateUp(Action onComplete = null)
    {
        m_IsAnimating = true;
        transform.DOLocalMoveY(transform.localPosition.y + GetRowHeight(), m_MoveYDuration)
            .OnComplete(() => {
                m_IsAnimating = false;
                onComplete?.Invoke();
            });
    }

    private void AnimateDown(Action onComplete = null)
    {
        m_IsAnimating = true;
        transform.DOLocalMoveY(transform.localPosition.y - GetRowHeight(), m_MoveYDuration)
            .OnComplete(() => {
                m_IsAnimating = false;
                onComplete?.Invoke();
            });
    }

    public bool IsFullyCleared()
    {
        if (m_Rows.Count <= 1)
        {
            return false;
        }

        // Don't count the top row
        for (int i = 1; i < m_Rows.Count; ++i)
        {
            if (!m_Rows[i].IsEmpty())
            {
                return false;
            }
        }
        return true;
    }

    public int GetNbFilledSlots()
    {
        int count = 0;
        for (int i = 0; i < m_Rows.Count; ++i)
        {
            for (int j = 0; j < m_Rows[i].Slots.Length; ++j)
            {
                if (m_Rows[i].Slots[j].Bubble != null)
                {
                    count++;
                }
            }
        }
        return count;
    }

    // Debug
    [Button]
    public void PrintNbFilledSlots()
    {
        Debug.Log("BubbleGrid: "+GetNbFilledSlots()+" slots filled.");
    }
}
