using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BubbleMerger : MonoBehaviour
{
    [SerializeField] private float m_MergeStepDuration = 0.5f;
    [SerializeField] private float m_BeforeMergeDelay = 0.2f;
    [SerializeField] private SplashTextPopup m_MergeComboPopupPrefab = null;

    [Space]
    [SerializeField] private bool m_Debug = false;
    [SerializeField] private bool m_DisplayDebugIds = false;
    [SerializeField] private bool m_AnimateMoves = true;
    [SerializeField] private float m_DebugMergeStepDuration = 0.5f;

    private bool m_Merging = false;
    private int m_Combo = 0;

    public bool IsMerging => m_Merging;

    public void Awake()
    {
        if (m_Debug)
        {
            m_MergeStepDuration = m_DebugMergeStepDuration;
        }
    }

    public void ComputeMerge(BubbleSlot slot)
    {
        // Find all connected nodes for the merge
        var mergeCluster = new List<BubbleSlot>();
        GetMergeCluster(slot, mergeCluster);
        if (mergeCluster.Count == 1)
        {
            return;
        }

        int maxLevel = slot.Bubble.MaxLevel;
        int mergeResultLevel = Mathf.Min(maxLevel, slot.Level + mergeCluster.Count - 1);
        //Debug.Log("Merge result level: " + Mathf.Pow(2, mergeResultLevel+1));

        if (m_Debug)
        {
            foreach (var tmpSlot in mergeCluster)
            {
                tmpSlot.Bubble.SetColor(Color.black);
            }
        }

        List<BubbleSlot> mergeDestinationCandidates = new List<BubbleSlot>();
        foreach (var mergeSlot in mergeCluster)
        {
            // Find ideal destinations that could lead to new merges
            foreach (var neighbour in mergeSlot.Neighbours)
            {
                // FIXME: some candidate merge distinations will not have a ceiling and fall..
                // Check if destination is attached, excluding the merge cluster
                if (neighbour != null && neighbour.Level == mergeResultLevel)
                {
                    mergeDestinationCandidates.Add(mergeSlot);
                }
            }
        }

        bool hasNewMergeOpportunity = mergeDestinationCandidates.Count > 0;

        if (mergeDestinationCandidates.Count == 0)
        {
            mergeDestinationCandidates.AddRange(mergeCluster);
        }

        // Merge in up direction
        BubbleSlot highestSlot = null;
        int highestRowId = -int.MaxValue;
        foreach (var candidate in mergeDestinationCandidates)
        {
            int rowId = candidate.Row.Id;
            if (rowId > highestRowId)
            {
                highestSlot = candidate;
                highestRowId = rowId;
            }
        }

        BubbleSlot mergeDestination = highestSlot;

        StartCoroutine(PlayMerge(mergeDestination, mergeResultLevel, mergeCluster, hasNewMergeOpportunity));
    }

    private IEnumerator PlayMerge(BubbleSlot mergeDestination, int resultLevel, List<BubbleSlot> mergeCluster, bool testNewMergeOpportunities)
    {
        m_Merging = true;

        int remainingDistance = PrepareMerge(mergeDestination);

        // make the animation stand out
        foreach (var slot in mergeCluster)
        {
            var pos = slot.Bubble.transform.position;
            pos.z -= 1;
            slot.Bubble.transform.position = pos;
        }

        yield return new WaitForSeconds(m_BeforeMergeDelay);

        // merge towards destination, from extremities of the cluster
        int traveledDistance = 0;
        while (remainingDistance > 0)
        {
            foreach (var slot in mergeCluster)
            {
                if (slot.Bubble != null && slot.Bubble.MergeDistance == remainingDistance)
                {
                    MergeOne(slot.Bubble, traveledDistance);
                }
            }
            remainingDistance--;
            traveledDistance++;

            yield return new WaitForSeconds(m_MergeStepDuration);
        }

        var resultSlot = mergeCluster.Find((x) => x.Bubble != null && x.Bubble.MergeDistance == 0);
        if (resultSlot != null && resultSlot.Bubble != null)
        {
            resultSlot.Bubble.SetLevel(resultLevel);
            resultSlot.Bubble.ResetState(); // reset for future merges

            resultSlot.Bubble.PlayMergeFX(traveledDistance);

            if (resultLevel >= resultSlot.Bubble.MaxLevel)
            {
                resultSlot.Bubble.Explode();
                FinishCombo();
            }
            else if (testNewMergeOpportunities)
            {
                m_Combo++;
                ComputeMerge(resultSlot);
            }
            else
            {
                FinishCombo();
            }
        }

        m_Merging = false;
    }

    private void FinishCombo()
    {
        if (m_Combo > 0)
        {
            var mergeComboPopup = Pools.GetFromPool(m_MergeComboPopupPrefab);
            mergeComboPopup.SetText("X" + (m_Combo+1));

            m_Combo = 0;
        }
    }

    private int PrepareMerge(BubbleSlot slot, int distance = 0)
    {
        //Debug.Log("PrepareMerge: "+slot.Bubble.Id + "; distance: " + distance);
        slot.Bubble.MergeDistance = distance;

        if (m_Debug)
        {
            string debugLabel = distance + ((m_DisplayDebugIds) ? "[" + slot.Bubble.Id + "]" : "");
            slot.Bubble.SetLabel(debugLabel);
        }

        int maxDistance = distance;
        List<BubbleSlot> validNeighbours = new List<BubbleSlot>();
        foreach (var neighbour in slot.Neighbours)
        {
            if (neighbour != null && !neighbour.Row.IsTopRow &&
                neighbour.Level == slot.Level && neighbour.Bubble.MergeDistance < 0)
            {
                neighbour.Bubble.MergeDistance = distance + 1;
                //Debug.Log(slot.Bubble.Id+" finds neighbour "+neighbour.Bubble.Id);
                validNeighbours.Add(neighbour);
            }
        }

        foreach (var neighbour in validNeighbours)
        {
            int branchDistance = PrepareMerge(neighbour, distance + 1);
            maxDistance = Mathf.Max(branchDistance, maxDistance);
        }

        return maxDistance;
    }

    private void MergeOne(Bubble bubble, int traveledDistance = 0)
    {
        if (m_Debug)
        {
            bubble.SetColor(Color.gray);
        }

        // merge to available neighbour
        foreach (var neighbour in bubble.Slot.Neighbours)
        {
            if (neighbour != null && neighbour.Bubble != null && 
                neighbour.Bubble.MergeDistance == bubble.MergeDistance - 1)
            {
                if (!(m_Debug && !m_AnimateMoves))
                {
                    bubble.PlayMergeAnim();
                    neighbour.Bubble.PlayMergeFX(traveledDistance);
                    neighbour.Bubble.PlayBumpAnim();

                    bubble.transform.DOMove(neighbour.transform.position, m_MergeStepDuration)
                        .OnComplete(() => {
                            OnMergeStepComplete(bubble);
                        });
                }
                else
                {
                    OnMergeStepComplete(bubble);
                }
                break;
            }
        }
    }

    private void OnMergeStepComplete(Bubble absorbedBubble)
    {
        Vibration.TriggerLightImpact();

        absorbedBubble.Slot.DestroyBubble();
        absorbedBubble.Slot.DetachDisconnectedNeighbours();
    }

    public void GetMergeCluster(BubbleSlot slot, List<BubbleSlot> mergeCluster)
    {
        mergeCluster.Add(slot);

        foreach (var neighbour in slot.Neighbours)
        {
            if (neighbour != null && !neighbour.Row.IsTopRow &&
                neighbour.Level == slot.Level && !mergeCluster.Contains(neighbour))
            {
                GetMergeCluster(neighbour, mergeCluster);
            }
        }
    }
}
