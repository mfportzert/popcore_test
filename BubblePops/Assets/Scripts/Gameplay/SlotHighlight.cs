using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotHighlight : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_SpriteRenderer = null;
    [SerializeField] private Animator m_Animator = null;

    private float m_Alpha;

    private void Awake()
    {
        m_Alpha = m_SpriteRenderer.color.a;
    }

    public void SetColor(Color color)
    {
        color.a = m_Alpha;
        m_SpriteRenderer.color = color;
    }

    public void Show()
    {
        m_Animator.SetTrigger("Show");
    }
}
