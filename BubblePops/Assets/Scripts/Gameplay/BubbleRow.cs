using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleRow : MonoBehaviour
{
    public int Id;
    public bool IsTopRow = false;
    public BubbleSlot[] Slots;

    public void Init(int id, int nbColumns)
    {
        Id = id;
        Slots = new BubbleSlot[nbColumns];
        name = "Row_" + Id;
    }

    public bool IsEmpty()
    {
        foreach (var slot in Slots)
        {
            if (slot.Bubble != null)
            {
                return false;
            }
        }
        return true;
    }

    public void Fill()
    {
        foreach (var slot in Slots)
        {
            slot.CreateBubble();
        }
    }

    public void SetupLinks(BubbleRow topRow, BubbleRow bottomRow)
    {
        for (int i = 0; i < Slots.Length; ++i)
        {
            Slots[i].Left = (i > 0) ? Slots[i - 1] : null;
            Slots[i].Right = (i < Slots.Length - 1) ? Slots[i + 1] : null;

            if (topRow != null)
            {
                if (Id % 2 == 0)
                {
                    Slots[i].TopLeft = topRow.Slots[i];
                    Slots[i].TopRight = (i < Slots.Length - 1) ? topRow.Slots[i + 1] : null;

                    topRow.Slots[i].BottomLeft = (i > 0) ? Slots[i - 1] : null;
                    topRow.Slots[i].BottomRight = Slots[i];
                }
                else
                {
                    Slots[i].TopLeft = (i > 0) ? topRow.Slots[i - 1] : null;
                    Slots[i].TopRight = topRow.Slots[i];

                    topRow.Slots[i].BottomLeft = Slots[i];
                    topRow.Slots[i].BottomRight = (i < Slots.Length - 1) ? Slots[i + 1] : null;
                }
            }

            if (bottomRow != null)
            {
                if (Id % 2 == 0)
                {
                    Slots[i].BottomLeft = bottomRow.Slots[i];
                    Slots[i].BottomRight = (i < Slots.Length - 1) ? bottomRow.Slots[i + 1] : null;

                    bottomRow.Slots[i].TopLeft = (i > 0) ? Slots[i - 1] : null;
                    bottomRow.Slots[i].TopRight = Slots[i];
                }
                else
                {
                    Slots[i].BottomLeft = (i > 0) ? bottomRow.Slots[i - 1] : null;
                    Slots[i].BottomRight = bottomRow.Slots[i];

                    bottomRow.Slots[i].TopLeft = Slots[i];
                    bottomRow.Slots[i].TopRight = (i < Slots.Length - 1) ? Slots[i + 1] : null;
                }
            }
        }
    }
}
