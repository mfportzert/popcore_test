using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DitzeGames.Effects;
using DG.Tweening;

public class Bubble : MonoBehaviour
{
    private static uint BubbleId = 0;

    [SerializeField] private BubbleConfig m_BubbleConfig = null;

    [Space]
    [SerializeField] private Animator m_Animator = null;
    [SerializeField] private MeshRenderer m_Renderer = null;
    [SerializeField] private TextMeshProUGUI m_Label = null;
    [SerializeField] private Collider m_Collider = null;
    [SerializeField] private TrailRenderer m_Trail = null;

    [Space]
    // Hack: downscale the 3d instead
    [SerializeField] private float m_GravityScale = 5.0f;

    [HideInInspector] public BubbleSlot Slot;
    [HideInInspector] public int MergeDistance = -1;

    private uint m_Id;
    private int m_Level = 0;
    private Rigidbody m_Rigidbody = null;
    private Vector3 m_MergeFxBaseScale;

    public uint Id => m_Id;
    public int Level => m_Level;
    public int MaxLevel => m_BubbleConfig.Levels.Length - 1;

    public void Awake()
    {
        int level = UnityEngine.Random.Range(0, 6);
        SetLevel(level);

        m_Id = BubbleId;
        name = "Bubble_" + BubbleId;
        BubbleId++;

        m_MergeFxBaseScale = m_BubbleConfig.MergeFXPrefab.transform.localScale;
    }

    public void SetLevel(int level)
    {
        m_Level = level;

        SetColor(m_BubbleConfig.GetColor(level));
        SetLabel(m_BubbleConfig.GetLabel(level));
    }

    public Color GetColor()
    {
        return m_BubbleConfig.GetColor(m_Level);
    }

    public void SetColor(Color color)
    {
        m_Renderer.material.SetColor("_Color", color);
    }

    public void SetLabel(string label)
    {
        m_Label.text = label;
    }

    // Called when hit by an explosion
    public void Pop()
    {
        if (Slot != null)
        {
            if (Slot.Row.IsTopRow)
            {
                return;
            }

            // TODO: replace by OnPopping event
            Slot.OnBubblePopping();
        }

        float animDelay = UnityEngine.Random.Range(0f, 0.25f);
        PlayPopAnim(animDelay);

        Destroy(m_BubbleConfig.PopDestroyDelay + animDelay);
    }

    public void PlayPopAnim(float delay = 0f)
    {
        StartCoroutine(PlayPopAnimCo(delay));
    }

    private IEnumerator PlayPopAnimCo(float delay)
    {
        yield return new WaitForSeconds(delay);
        PlayPopFX();
        m_Animator.SetTrigger("Pop");
    }

    public void Drop()
    {
        PlayFallFX();

        transform.DOShakePosition(0.2f, 0.1f, 30)
            .OnComplete(() => Fall());
    }

    private void Fall()
    {
        Vibration.TriggerLightImpact();

        // TODO: create config
        Vector3 randomForce = new Vector3(
            UnityEngine.Random.Range(-3f, 3f),
            UnityEngine.Random.Range(8.5f, 12.5f),
            0);

        m_Rigidbody = gameObject.AddComponent<Rigidbody>();
        m_Rigidbody.AddForce(randomForce, ForceMode.Impulse);

        m_Collider.enabled = false;
        m_Trail.enabled = false;

        Destroy(3f);
    }

    private void FixedUpdate()
    {
        if (m_Rigidbody != null)
        {
            Vector3 gravity = Physics.gravity * m_GravityScale;
            m_Rigidbody.AddForce(gravity, ForceMode.Acceleration);
        }
    }

    public void PlayMergeAnim()
    {
        m_Animator.SetTrigger("Absorbed");
    }

    public void PlayBumpAnim()
    {
        m_Animator.SetTrigger("Bump");
    }

    public void PlayAppearAnim()
    {
        m_Animator.SetTrigger("Appear");
    }

    public void PlayBounceAnim()
    {
        m_Animator.SetTrigger("Bounce");
    }

    public void Explode()
    {
        PlayExplosionFX();
        CameraShake.ShakeOnce();
        Vibration.TriggerMediumImpact();

        //Debug.LogError("Explode!");
        float radius = m_BubbleConfig.ExplosionRadius;

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        foreach (var collider in hitColliders)
        {
            Bubble hitBubble;
            collider.TryGetComponent<Bubble>(out hitBubble);

            if (hitBubble != null && hitBubble != this)
            {
                hitBubble.Pop();
            }
        }

        Pop();
    }

    public void PlayExplosionFX()
    {
        var fx = Pools.GetFromPool(m_BubbleConfig.ExplosionFXPrefab);

        SetupFxPosition(fx);

        fx.Play();
    }

    public void PlayMergeFX(int traveledDistance)
    {
        var fx = Pools.GetFromPool(m_BubbleConfig.MergeFXPrefab);

        SetupFxPosition(fx);
        SetFxColor(fx, GetColor(), false);

        // the longer the merge, the bigger the fx
        fx.transform.localScale = m_MergeFxBaseScale * (1f + 0.1f * traveledDistance);

        fx.Play();
    }

    public void PlayPopFX()
    {
        var fx = Pools.GetFromPool(m_BubbleConfig.PopFXPrefab);

        SetupFxPosition(fx);
        SetFxColor(fx, GetColor(), false);

        fx.Play();
    }

    public void PlayFallFX()
    {
        var fx = Pools.GetFromPool(m_BubbleConfig.FallFXPrefab);

        SetupFxPosition(fx);
        SetFxColor(fx, GetColor(), false);

        fx.Play();
    }

    private void SetupFxPosition(ParticleSystem ps)
    {
        var pos = ps.transform.position;
        pos.x = transform.position.x;
        pos.y = transform.position.y;
        ps.transform.position = pos;
    }

    private void SetFxColor(ParticleSystem ps, Color color, bool setChildren = false)
    {
        var main = ps.main;
        main.startColor = color;

        if (setChildren)
        {
            var childrenPs = ps.GetComponentsInChildren<ParticleSystem>();
            foreach (var childPs in childrenPs)
            {
                main = childPs.main;
                main.startColor = color;
            }
        }
    }

    public void ResetState()
    {
        MergeDistance = -1;

        var pos = transform.position;
        pos.z = 0;
        transform.position = pos;
    }

    public void Destroy(float delay = 0f)
    {
        GameObject.Destroy(gameObject, delay);
    }
}
