using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class BubbleShooter : MonoBehaviour
{
    private static int BubbleLayer;

    [SerializeField] private Camera m_Camera = null;
    [SerializeField] private BubbleGrid m_BubbleGrid = null;
    [SerializeField] private BubbleMerger m_Merger = null;
    [SerializeField] private Bubble m_BubblePrefab = null;
    [SerializeField] private Transform m_BulletContainer = null;
    [SerializeField] private LineRenderer m_AimingLineRenderer = null;
    [SerializeField] private LayerMask m_ContactLayers = default;

    [Header("Shoot config")]
    [SerializeField] private float m_ShootSpeed = 6f;
    [SerializeField] private int m_MaxBounces = 1;

    [Header("Effects")]
    [SerializeField] private SlotHighlight m_SlotHighlightPrefab = null;
    [SerializeField] private SplashTextPopup m_MergeComboPopupPrefab = null;

    public enum EState
    {
        Idle = 0,
        Aiming = 1,
        Shooting = 2
    }

    private EState m_State = EState.Idle;
    private Bubble m_BubbleBullet;
    private Vector2 m_TouchPos;

    private Path m_Trajectory = new Path();
    private BubbleSlot m_TargetSlot = null;

    private SlotHighlight m_SlotHighlight;
    private bool m_ShootingActive = true;

    private void Awake()
    {
        BubbleLayer = LayerMask.NameToLayer("Bubble");

        InputEvents.TouchStart += OnTouchStart;
        InputEvents.TouchMove += OnTouchMove;
        InputEvents.TouchRelease += OnTouchRelease;

        BubbleSlot.OnCleared += OnSlotCleared;

        m_SlotHighlight = GameObject.Instantiate(m_SlotHighlightPrefab);
        ResetHighlight();
    }

    private void OnDestroy()
    {
        InputEvents.TouchStart -= OnTouchStart;
        InputEvents.TouchMove -= OnTouchMove;
        InputEvents.TouchRelease -= OnTouchRelease;

        BubbleSlot.OnCleared -= OnSlotCleared;
    }

    private void Start()
    {
        CreateBubbleBullet();
    }

    private void CreateBubbleBullet()
    {
        m_BubbleBullet = GameObject.Instantiate(m_BubblePrefab, m_BulletContainer);
    }

    private void UpdateAiming()
    {
        if (m_State == EState.Aiming)
        {
            Vector3 aimPos = m_Camera.ScreenToWorldPoint(m_TouchPos);
            aimPos.z = m_BubbleBullet.transform.position.z;

            Vector3 startPos = m_BubbleBullet.transform.position;
            Vector3 aimDirection = (aimPos - startPos).normalized;

            //Debug.Log("AimDir " + aimDirection);
            if (Mathf.Abs(aimDirection.x) < 0.95f && aimDirection.y > 0.1f)
            {
                ComputeTrajectory(startPos, aimDirection);
            }

            if (m_TargetSlot != null)
            {
                HighlightSlot(m_TargetSlot);
            }
            else
            {
                ResetTrajectory();
                ResetHighlight();
            }

            UpdateAimingLine();
        }
    }

    private void ComputeTrajectory(Vector3 startPos, Vector3 aimDirection)
    {
        m_TargetSlot = null;

        m_Trajectory.Clear();
        m_Trajectory.AddPoint(startPos);

        int bouncesCount = 0;
        Vector3 stepPos = startPos;
        while (bouncesCount <= m_MaxBounces)
        {
            RaycastHit hit;
            if (Physics.Raycast(stepPos, aimDirection, out hit, 100f, m_ContactLayers))
            {
                //Debug.Log("Hit "+ bouncesCount + ": "+hit.collider.gameObject.name+"; "+hit.point);
                bouncesCount++;

                stepPos = hit.point;
                aimDirection.x = -aimDirection.x;

                m_Trajectory.AddPoint(hit.point);

                if (hit.collider.gameObject.layer == BubbleLayer)
                {
                    Bubble bubble = hit.collider.gameObject.GetComponent<Bubble>();
                    m_TargetSlot = (hit.normal.x <= 0) ? bubble.Slot.BottomLeft : bubble.Slot.BottomRight;
                    if (m_TargetSlot != null && m_TargetSlot.Bubble != null)
                    {
                        m_TargetSlot = (m_TargetSlot == bubble.Slot.BottomLeft) ? bubble.Slot.BottomRight : bubble.Slot.BottomLeft;
                    }
                    break;
                }
            }
            else
            {
                m_TargetSlot = null;
                break;
            }
        }
    }

    private void ResetTrajectory()
    {
        m_Trajectory.Clear();
        m_TargetSlot = null;
    }

    private void HighlightSlot(BubbleSlot slot)
    {
        if (slot != null)
        {
            m_SlotHighlight.gameObject.SetActive(true);
            m_SlotHighlight.transform.position = slot.transform.position;
            m_SlotHighlight.SetColor(m_BubbleBullet.GetColor());
            m_SlotHighlight.Show();
        }
    }

    private void ResetHighlight()
    {
        m_SlotHighlight.gameObject.SetActive(false);
    }

    private void UpdateAimingLine()
    {
        if (m_State == EState.Aiming)
        {
            Vector3[] positions = new Vector3[m_Trajectory.Points.Count];
            for (int i = 0; i < m_Trajectory.Points.Count; ++i)
            {
                positions[i] = m_Trajectory.Points[i];
            }

            m_AimingLineRenderer.gameObject.SetActive(true);
            m_AimingLineRenderer.positionCount = positions.Length;
            m_AimingLineRenderer.SetPositions(positions);
        }
        else
        {
            m_AimingLineRenderer.gameObject.SetActive(false);
        }
    }

    private void TryAiming()
    {
        if (m_State == EState.Idle)
        {
            m_State = EState.Aiming;
            UpdateAiming();
        }
    }

    private void TryShooting()
    {
        if (m_State == EState.Shooting)
        {
            return;
        }

        if (m_State == EState.Aiming && m_TargetSlot != null && m_TargetSlot.HasConnectedCeiling() &&
            m_ShootingActive && !m_BubbleGrid.IsAnimating && !m_Merger.IsMerging)
        {
            Shoot();
        }
        else
        {
            ResetTrajectory();
            ResetHighlight();
            m_State = EState.Idle;
        }

        UpdateAimingLine();
    }

    private void Shoot()
    {
        m_State = EState.Shooting;

        Vibration.TriggerLightImpact();

        // snap destination to target slot
        m_Trajectory.Points[m_Trajectory.Points.Count - 1] = m_TargetSlot.transform.position;

        m_Trajectory.FollowPath(m_BubbleBullet, m_ShootSpeed, OnShootFinished);

        ResetHighlight();
    }

    private void OnShootFinished(Bubble shotBubble)
    {
        m_TargetSlot.SetBubble(m_BubbleBullet);
        m_TargetSlot.CreateImpactShockwave();

        shotBubble.PlayBounceAnim();

        m_Merger.ComputeMerge(m_TargetSlot);

        ReloadBullet();

        m_BubbleGrid.UpdateContent();
        m_State = EState.Idle;
    }

    private void ReloadBullet()
    {
        CreateBubbleBullet();
    }

    private void CheckPerfect()
    {
        if (m_BubbleGrid.IsFullyCleared())
        {
            StartCoroutine(PlayPerfectSequence());
        }
    }

    private IEnumerator PlayPerfectSequence()
    {
        m_ShootingActive = false;

        yield return new WaitForSeconds(0.3f);

        var mergeComboPopup = Pools.GetFromPool(m_MergeComboPopupPrefab);
        mergeComboPopup.SetText("Perfect");

        yield return new WaitForSeconds(0.7f);

        m_BubbleGrid.ResetGrid();
        m_ShootingActive = true;
    }

    #region events

    private void OnTouchStart(Vector2 pos)
    {
        m_TouchPos = pos;

        TryAiming();

        Vibration.TriggerLightImpact();
    }

    private void OnTouchMove(Vector2 newPosition, Vector2 delta)
    {
        m_TouchPos = newPosition;

        UpdateAiming();
    }

    private void OnTouchRelease(Vector2 pos, Vector2 delta)
    {
        TryShooting();
    }

    private void OnSlotCleared(BubbleSlot slot)
    {
        CheckPerfect();
    }

    #endregion
}
