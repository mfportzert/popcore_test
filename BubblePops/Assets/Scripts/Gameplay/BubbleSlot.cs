using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class BubbleSlot : MonoBehaviour
{
    public static Action<BubbleSlot> OnCleared;

    [SerializeField] private Bubble m_BubblePrefab = null;

    [Header("Debug Display")]
    [SerializeField] private BubbleSlot m_DebugLeft;
    [SerializeField] private BubbleSlot m_DebugTopLeft;
    [SerializeField] private BubbleSlot m_DebugTopRight;
    [SerializeField] private BubbleSlot m_DebugRight;
    [SerializeField] private BubbleSlot m_DebugBottomRight;
    [SerializeField] private BubbleSlot m_DebugBottomLeft;

    [HideInInspector] public BubbleRow Row; 

    private Bubble m_Bubble;
    private BubbleSlot[] m_Neighbours;

    public Bubble Bubble => m_Bubble;
    public int Level => (m_Bubble) ? m_Bubble.Level : -1;
    public BubbleSlot[] Neighbours => m_Neighbours;

    public BubbleSlot Left          { get { return m_Neighbours[0]; } set { m_Neighbours[0] = value; m_DebugLeft = value; } }
    public BubbleSlot TopLeft       { get { return m_Neighbours[1]; } set { m_Neighbours[1] = value; m_DebugTopLeft = value; } }
    public BubbleSlot TopRight      { get { return m_Neighbours[2]; } set { m_Neighbours[2] = value; m_DebugTopRight = value; } }
    public BubbleSlot Right         { get { return m_Neighbours[3]; } set { m_Neighbours[3] = value; m_DebugRight = value; } }
    public BubbleSlot BottomRight   { get { return m_Neighbours[4]; } set { m_Neighbours[4] = value; m_DebugBottomRight = value; } }
    public BubbleSlot BottomLeft    { get { return m_Neighbours[5]; } set { m_Neighbours[5] = value; m_DebugBottomLeft = value; } }

    private void Awake()
    {
        m_Neighbours = new BubbleSlot[6];
    }

    public void CreateBubble()
    {
        if (m_Bubble != null)
        {
            return;
        }

        m_Bubble = GameObject.Instantiate(m_BubblePrefab, transform);
        m_Bubble.transform.localPosition = Vector3.zero;
        m_Bubble.Slot = this;

        m_Bubble.PlayAppearAnim();
    }

    public void SetBubble(Bubble bubble)
    {
        m_Bubble = bubble;
        m_Bubble.Slot = this;

        m_Bubble.transform.SetParent(transform);
        m_Bubble.transform.localPosition = Vector3.zero;
    }

    public void DestroyBubble()
    {
        if (m_Bubble != null)
        {
            m_Bubble.Destroy();
            ClearSlot();
        }
    }

    public void PopBubble()
    {
        if (m_Bubble != null)
        {
            m_Bubble.Pop();
        }
    }

    public void OnBubblePopping()
    {
        ClearSlot();
        DetachDisconnectedNeighbours();
    }

    public void DetachDisconnectedNeighbours()
    {
        BottomLeft?.TryDrop();
        BottomRight?.TryDrop();
        Left?.TryDrop();
        Right?.TryDrop();
    }

    public void TryDrop()
    {
        if (m_Bubble == null)
        {
            return;
        }

        List<BubbleSlot> slotsCluster; // self included
        if (!HasConnectedCeiling(out slotsCluster))
        {
            //if (m_Debug)
            //{
            //    slot.Bubble.SetColor(Color.red);
            //}

            foreach (var slot in slotsCluster)
            {
                slot.DropBubble();
            }
        }
    }

    public void DropBubble()
    {
        m_Bubble.Drop();
        ClearSlot();
    }

    public bool HasConnectedCeiling()
    {
        return HasConnectedCeiling(out _);
    }

    public bool HasConnectedCeiling(out List<BubbleSlot> testedSlots)
    {
        List<BubbleSlot> openSlots = new List<BubbleSlot> { this };
        testedSlots = new List<BubbleSlot>();

        while (openSlots.Count > 0)
        {
            var currSlot = openSlots[0];
            openSlots.RemoveAt(0);
            testedSlots.Add(currSlot);

            foreach (var neighbour in currSlot.Neighbours)
            {
                if (neighbour != null && neighbour.Bubble != null &&
                    !openSlots.Contains(neighbour) && !testedSlots.Contains(neighbour))
                {
                    openSlots.Add(neighbour);
                }
            }

            if (currSlot.Row.IsTopRow)
            {
                return true;
            }
        }
        return false;
    }

    private void ClearSlot()
    {
        m_Bubble = null;

        OnCleared?.Invoke(this);
    }

    public void CreateImpactShockwave()
    {
        foreach (var neighbour in m_Neighbours)
        {
            if (neighbour != null && neighbour.Bubble != null)
            {
                Vector3 deltaPos = neighbour.transform.position - transform.position;
                Vector3 direction = deltaPos.normalized;
                Vector3 destination = neighbour.transform.localPosition + direction * (deltaPos.magnitude * 0.2f);

                neighbour.transform.DOLocalMove(destination, 0.1f).SetEase(Ease.OutQuad).SetLoops(2, LoopType.Yoyo);
            }
        }
    }
}
