﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(InputHandler))]
public class InputEvents : MonoBehaviour
{
    public static Action<Vector2> TouchStart; // Vector2 position
    public static Action<Vector2> TouchStationary; // Vector2 position
    public static Action<Vector2, Vector2> TouchMove; // Vector2 newPosition, Vector2 delta
    public static Action<Vector2, Vector2> TouchRelease; // Vector2 position, Vector2 delta
    public static Action<Vector2> Tap; // Vector2 position
    public static Action<Vector2> DoubleTap; // Vector2 position
    public static Action<Vector2, Vector2, float> Swipe; // Vector2 startPosition, Vector2 endPosition, float duration

    private InputHandler m_InputHandler = null;

    private void Awake()
    {
        m_InputHandler = GetComponent<InputHandler>();

        m_InputHandler.OnTouchStart += OnTouchStart;
        m_InputHandler.OnTouchRelease += OnTouchRelease;
        m_InputHandler.OnTouchMove += OnTouchMove;
        m_InputHandler.OnTouchStationary += OnTouchStationary;
        m_InputHandler.OnTap += OnTap;
        m_InputHandler.OnDoubleTap += OnDoubleTap;
        m_InputHandler.OnSwipe += OnSwipe;
    }

    private void OnDestroy()
    {
        m_InputHandler.OnTouchStart -= OnTouchStart;
        m_InputHandler.OnTouchRelease -= OnTouchRelease;
        m_InputHandler.OnTouchMove -= OnTouchMove;
        m_InputHandler.OnTouchStationary -= OnTouchStationary;
        m_InputHandler.OnTap -= OnTap;
        m_InputHandler.OnDoubleTap -= OnDoubleTap;
        m_InputHandler.OnSwipe -= OnSwipe;
    }

    private void OnTouchStart(Vector2 position)
    {
        TouchStart?.Invoke(position);
    }

    private void OnTouchStationary(Vector2 position)
    {
        TouchStationary?.Invoke(position);
    }

    private void OnTouchRelease(Vector2 position, Vector2 delta)
    {
        TouchRelease?.Invoke(position, delta);
    }

    private void OnTouchMove(Vector2 position, Vector2 delta)
    {
        TouchMove?.Invoke(position, delta);
    }

    private void OnTap(Vector2 position)
    {
        Tap?.Invoke(position);
    }

    private void OnDoubleTap(Vector2 position)
    {
        DoubleTap?.Invoke(position);
    }

    private void OnSwipe(Vector2 startPosition, Vector2 endPosition, float duration)
    {
        Swipe?.Invoke(startPosition, endPosition, duration);
    }
}
