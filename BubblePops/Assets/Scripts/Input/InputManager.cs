﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : Singleton<InputManager>
{
    [SerializeField] private InputHandler m_InputHandler = null;

    public InputHandler InputHandler
    {
        get { return m_InputHandler; }
    }

    public Vector3 GetCurrentTouchPos()
    {
        return m_InputHandler.GetCurrentTouchPos();
    }

    public bool IsPointerOverUI()
    {
        int pointerId = m_InputHandler.CurrentPointerId;
        return EventSystem.current.IsPointerOverGameObject(pointerId);
    }

    // Mostly for editor debug purpose
    public GameObject GetGameObjectUnderPointer()
    {
        int pointerId = m_InputHandler.CurrentPointerId;

        PointerEventData pointer = new PointerEventData(EventSystem.current);
#if UNITY_EDITOR
        pointer.position = Input.mousePosition;
#else
        pointer.position = Input.GetTouch(pointerId).position;
#endif

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointer, raycastResults);

        GameObject result = null;
        if (raycastResults.Count > 0)
        {
            foreach (var hit in raycastResults)
            {
                result = hit.gameObject;
                break;
            }
        }
        return result;
    }

    public void PrintGameObjectUnderPointer()
    {
        GameObject obj = GetGameObjectUnderPointer();
        if (obj != null)
        {
            Debug.LogError("GameObject under pointer: "+obj.name);
        }
    }
}
