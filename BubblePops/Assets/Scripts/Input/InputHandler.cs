﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputHandler : MonoBehaviour
{
    [Header("Tap")]
    [SerializeField] private float m_TapMaxDuration = 0.2f;
    [SerializeField] private float m_TapCancelDistance = 20f;

    //[Header("Double Tap")]
    //[SerializeField] private float m_DoubleTapMaxDuration = 0.5f;

    [Header("Swipe")]
    [SerializeField] private float m_SwipeMaxDuration = 0.6f;
    [SerializeField] private float m_SwipeMinDistance = 20f;

    #region Callbacks
    public Action<Vector2> OnTouchStart = null;
    public Action<Vector2> OnTouchStationary = null;
    public Action<Vector2, Vector2> OnTouchRelease = null;
    public Action<Vector2, Vector2> OnTouchMove = null; // newPos, moveDelta

    public Action<Vector2, Vector2, float> OnSwipe = null; // startPos, endPos

    public Action<Vector2> OnTap = null;
    public Action<Vector2> OnDoubleTap = null;
    #endregion

    private Vector2 m_StartInputPos = Vector2.zero;
    private float m_StartInputTime = 0f;

    private Vector2 m_LastInputPos = Vector2.zero;
    private Vector2 m_LastMoveDelta = Vector2.zero;

    private int m_CurrentFingerId = -1;

    public int CurrentPointerId => m_CurrentFingerId;

    public Vector2 GetCurrentTouchPos()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        return Input.mousePosition;
#elif UNITY_ANDROID || UNITY_IOS
        //Debug.LogError("CurrentFingerId: "+m_CurrentFingerId);
        return GetTouchByFingerId(m_CurrentFingerId).position;
#endif
    }

    private Touch GetTouchByFingerId(int fingerId)
    {
        //Debug.LogError("Looking for finger Id");
        for (int i = 0; i < Input.touchCount; ++i)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.fingerId == fingerId)
            {
                //Debug.LogError("Finger Id found");
                return touch;
            }
        }
        return default(Touch);
    }

    private void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        HandleMouseInput();
#elif UNITY_ANDROID || UNITY_IOS
        HandleTouchInput();
#endif
    }

    private void HandleMouseInput()
    {
        bool mouseDown = Input.GetMouseButtonDown(0);
        bool mousePressed = Input.GetMouseButton(0);
        bool mouseUp = Input.GetMouseButtonUp(0);

        Vector2 currentPos = Input.mousePosition;

        if (mouseDown)
        {
            HandleTouchStarted(currentPos);
        }
        else if (mousePressed)
        {
            HandleTouchMoved(currentPos);
        }
        else if (mouseUp)
        {
            HandleTouchReleased(currentPos);
        }
    }

    private void HandleTouchInput()
    {
        for (int i = 0; i < Input.touchCount; ++i)
        {
            Touch touch = Input.GetTouch(i);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (m_CurrentFingerId == -1)
                    {
                        m_CurrentFingerId = touch.fingerId;
                        HandleTouchStarted(touch.position);
                    }
                    break;

                case TouchPhase.Ended:
                    HandleTouchReleased(touch.position);
                    m_CurrentFingerId = -1;
                    break;

                case TouchPhase.Stationary:
                    HandleTouchStationary(touch.position);
                    break;

                case TouchPhase.Moved:
                    HandleTouchMoved(touch.position);
                    break;
            }
        }
    }

    private void HandleTouchStarted(Vector2 position)
    {
        OnTouchStart?.Invoke(position);

        m_StartInputTime = Time.time;
        m_StartInputPos = position;
        m_LastInputPos = position;
    }

    private void HandleTouchStationary(Vector2 position)
    {
        OnTouchStationary?.Invoke(position);
    }

    private void HandleTouchMoved(Vector2 position)
    {
        if (m_LastInputPos == position)
        {
            HandleTouchStationary(position);
            return;
        }

        m_LastMoveDelta = position - m_LastInputPos;
        m_LastInputPos = position;

        OnTouchMove?.Invoke(position, m_LastMoveDelta);
    }

    private void HandleTouchReleased(Vector2 position)
    {
        Vector2 lastMoveDelta = position - m_LastInputPos;
        OnTouchRelease?.Invoke(position, lastMoveDelta);

        float pressDuration = Time.time - m_StartInputTime;
        float distance = (m_StartInputPos - position).magnitude;
        if (pressDuration <= m_TapMaxDuration && distance < m_TapCancelDistance)
        {
            OnTap?.Invoke(position);
        }
        else if (pressDuration <= m_SwipeMaxDuration && distance > m_SwipeMinDistance)
        {
            OnSwipe?.Invoke(m_StartInputPos, position, pressDuration);
        }
    }
}
