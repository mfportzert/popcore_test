using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Path
{
    private List<Vector3> m_PathPoints = new List<Vector3>();

    public List<Vector3> Points => m_PathPoints;

    public void AddPoint(Vector3 pos)
    {
        m_PathPoints.Add(pos);
    }

    public void Clear()
    {
        m_PathPoints.Clear();
    }

    public float GetPathLength()
    {
        float pathLength = 0f;
        for (int i = 1; i < m_PathPoints.Count; ++i)
        {
            pathLength += (m_PathPoints[i] - m_PathPoints[i - 1]).magnitude;
        }
        return pathLength;
    }

    public void FollowPath<T>(T obj, float speed, Action<T> onFinish) where T : MonoBehaviour
    {
        if (m_PathPoints.Count == 0)
        {
            Debug.LogWarning("FollowPath: No points in path.");
        }

        obj.StartCoroutine(FollowPathCo<T>(obj, speed, onFinish));
    }

    private IEnumerator FollowPathCo<T>(T obj, float speed, Action<T> onFinish) where T : MonoBehaviour
    {
        Transform objTransform = obj.transform;
        float pathLength = GetPathLength();

        float distance = 0;
        while (distance < pathLength)
        {
            objTransform.position = GetPositionInPath(distance);

            distance += speed * Time.deltaTime;
            yield return 0;
        }

        onFinish.Invoke(obj);
    }

    public Vector3 GetPositionInPath(float distance)
    {
        if (m_PathPoints.Count == 0) return Vector3.zero;
        if (m_PathPoints.Count == 1) return m_PathPoints[0];

        float pathLength = GetPathLength();
        if (distance >= pathLength)
        {
            return m_PathPoints[m_PathPoints.Count - 1];
        }

        int pointIndex = 0;
        float distanceToGo = distance;
        Vector3 position = m_PathPoints[m_PathPoints.Count - 1];

        while (distanceToGo >= 0 && pointIndex < m_PathPoints.Count - 1)
        {
            var nextPoint = m_PathPoints[pointIndex + 1];
            var currentPoint = m_PathPoints[pointIndex];
            float segmentLength = (nextPoint - currentPoint).magnitude;

            if (distanceToGo < segmentLength)
            {
                Vector3 segmentDirection = (nextPoint - currentPoint).normalized;
                position = currentPoint + distanceToGo * segmentDirection;
                break;
            }

            distanceToGo -= segmentLength;
            pointIndex++;
        }

        return position;
    }
}
