using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SplashTextPopup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_Text = null;

    public void SetText(string text)
    {
        m_Text.text = text;
    }
}
