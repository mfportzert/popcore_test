using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Data/BubbleLevelConfig", fileName = "BubbleLevelConfig")]
public class BubbleLevelConfig : ScriptableObject
{
    [SerializeField] public Color Color;
}
