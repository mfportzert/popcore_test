using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BubbleConfig))]
public class BubbleConfigEditor : Editor
{
    SerializedProperty Levels;

    private void OnEnable()
    {
        Levels = serializedObject.FindProperty("Levels");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        EditorGUILayout.Space();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Colors Preview", EditorStyles.boldLabel);

        for (int i = 0; i < Levels.arraySize; ++i)
        {
            EditorGUILayout.BeginHorizontal();

            var levelProp = Levels.GetArrayElementAtIndex(i);
            if (levelProp.objectReferenceValue != null)
            {
                BubbleLevelConfig levelConf = (BubbleLevelConfig)levelProp.objectReferenceValue;
                EditorGUILayout.LabelField(levelConf.name, GUILayout.MaxWidth(64));

                EditorGUI.BeginChangeCheck();
                Color color = EditorGUILayout.ColorField(levelConf.Color);
                if (EditorGUI.EndChangeCheck())
                {
                    levelConf.Color = color;
                    EditorUtility.SetDirty(levelConf);
                }

                Levels.GetArrayElementAtIndex(i).objectReferenceValue = levelConf;
            }

            EditorGUILayout.EndHorizontal();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
