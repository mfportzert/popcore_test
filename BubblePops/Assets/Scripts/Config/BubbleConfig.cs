using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/BubbleConfig", fileName = "BubbleConfig")]
public class BubbleConfig : ScriptableObject
{
    public BubbleLevelConfig[] Levels;

    [Header("FXs")]
    public ParticleSystem MergeFXPrefab = null;
    public ParticleSystem PopFXPrefab = null;
    public ParticleSystem FallFXPrefab = null;
    public ParticleSystem ExplosionFXPrefab = null;

    [Header("Misc")]
    public float ExplosionRadius = 4;
    public float PopDestroyDelay = 0.15f;

    private bool IsLevelSupported(int level)
    {
        return level >= 0 && level < Levels.Length && Levels[level] != null;
    }

    public Color GetColor(int level)
    {
        return (IsLevelSupported(level)) ? Levels[level].Color : Color.black;
    }

    public string GetLabel(int level)
    {
        return (IsLevelSupported(level)) ? Levels[level].name : "??";
    }
}